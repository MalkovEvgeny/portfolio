package ru.inno.game.services;

import ru.inno.game.dto.StatisticDto;
import ru.inno.game.models.Game;
import ru.inno.game.models.Player;
import ru.inno.game.models.Shot;
import ru.inno.game.repository.GamesRepository;
import ru.inno.game.repository.PlayersRepository;
import ru.inno.game.repository.ShotsRepository;

import java.time.Duration;
import java.time.LocalDateTime;

public class GameServiceImpl implements GameService {
    private PlayersRepository playersRepository;
    private GamesRepository gamesRepository;
    private ShotsRepository shotsRepository;

    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname) {
        System.out.println("ПОЛУЧИЛИ " + firstIp + " " + secondIp + " " + firstPlayerNickname + " " + secondPlayerNickname);
        Player first = checkIfExists(firstIp, firstPlayerNickname);
        Player second = checkIfExists(secondIp, secondPlayerNickname);

        Game game = Game.builder()
                .dateTime(LocalDateTime.now())
                .playerFirst(first)
                .playerSecond(second)
                .playerFirstShotsCount(0)
                .playerSecondShotsCount(0)
                .secondsGameTimeAmount(0L)
                .build();
        gamesRepository.save(game);

        return game.getId();
    }

    private Player checkIfExists(String ip, String nickname) {
        Player player = playersRepository.findByNickname(nickname);
        if (player == null) {
            player = Player.builder()
                    .ip(ip)
                    .name(nickname)
                    .points(0)
                    .maxWinsCount(0)
                    .maxLosesCount(0)
                    .build();
            playersRepository.save(player);
        } else {
            player.setIp(ip);
            playersRepository.update(player);
        }
        return player;
    }

    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        Player shooter = playersRepository.findByNickname(shooterNickname);
        Player target = playersRepository.findByNickname(targetNickname);
        Game game = gamesRepository.findById(gameId);
        Shot shot = Shot.builder()
                .dateTime(LocalDateTime.now())
                .game(game)
                .shooter(shooter)
                .target(target)
                .build();

        shooter.setPoints(shooter.getPoints() + 1);

        checkShooter(shooterNickname, game);

        playersRepository.update(shooter);
        gamesRepository.update(game);
        shotsRepository.save(shot);
    }



    @Override
    public StatisticDto finishGame(Long gameId, Long seconds){
        Game game = gamesRepository.findById(gameId);
        game.setSecondsGameTimeAmount(seconds);

        String first = game.getPlayerFirst().getName();
        String second = game.getPlayerSecond().getName();
        Integer firstShotsCount  = game.getPlayerFirstShotsCount();
        Integer secondShotsCount = game.getPlayerSecondShotsCount();
        Integer firstAllCount = playersRepository.findByNickname(first).getPoints();
        Integer secondAllCount = playersRepository.findByNickname(second).getPoints();
        Long gameTime = game.getSecondsGameTimeAmount();

        String winner = addWinsAndLosesCount(game, first, second, firstShotsCount, secondShotsCount);

        StatisticDto statisticDto = new StatisticDto(first, second, firstShotsCount, secondShotsCount, firstAllCount, secondAllCount, winner, gameId, gameTime);
        playersRepository.update(game.getPlayerFirst());
        playersRepository.update(game.getPlayerSecond());
        gamesRepository.update(game);
        return statisticDto;
    }

    private String addWinsAndLosesCount(Game game, String first, String second, Integer firstShotsCount, Integer secondShotsCount) {
        String winner = getWinner(first, second, firstShotsCount, secondShotsCount);
        if (winner.equals(first)){
            game.getPlayerFirst().setMaxWinsCount(game.getPlayerFirst().getMaxWinsCount() + 1);
            game.getPlayerSecond().setMaxLosesCount(game.getPlayerSecond().getMaxLosesCount() + 1);
        } else if (winner.equals(second)){
            game.getPlayerSecond().setMaxWinsCount(game.getPlayerSecond().getMaxWinsCount() + 1);
            game.getPlayerFirst().setMaxLosesCount(game.getPlayerFirst().getMaxLosesCount() + 1);
        }
        return winner;
    }

    private String getWinner(String first, String second, Integer firstShotsCount, Integer secondShotsCount) {
        if (firstShotsCount > secondShotsCount) {
            return first;
        } else if (firstShotsCount < secondShotsCount){
            return second;
        } else {
            String result = "ничья";
            return result;
        }
    }

    private void checkShooter(String shooterNickname, Game game) {
        if (game.getPlayerFirst().getName().equals(shooterNickname)){
            game.setPlayerFirstShotsCount(game.getPlayerFirstShotsCount() + 1);
        }
        if (game.getPlayerSecond().getName().equals(shooterNickname)){
            game.setPlayerSecondShotsCount(game.getPlayerSecondShotsCount() + 1);
        }
    }
}
