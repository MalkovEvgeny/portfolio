package ru.inno.game.client.controllers;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import ru.inno.game.client.socket.SocketClient;
import ru.inno.game.client.utils.GameUtils;

import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    private SocketClient socketClient;

    private GameUtils gameUtils;

    @FXML
    private Circle player;

    @FXML
    private Circle enemy;
    @FXML
    private TextField textPlayerName;
    @FXML
    private Button buttonGo;
    @FXML
    private Button buttonConnect;
    @FXML
    private AnchorPane pane;
    @FXML
    private Label hpPlayer;
    @FXML
    private Label hpEnemy;
    @FXML
    private Label finishStatistic;



    public GameUtils getGameUtils() {
        return gameUtils;
    }

    public EventHandler<KeyEvent> getKeyEventEventHandler(){
        return keyEventEventHandler;
    }

    private EventHandler<KeyEvent> keyEventEventHandler = event -> {
      if (event.getCode() == KeyCode.RIGHT){
          gameUtils.goRight(player);
          socketClient.sendMessage("right");
      } else if(event.getCode() == KeyCode.LEFT){
          gameUtils.goLeft(player);
          socketClient.sendMessage("left");
      } else if(event.getCode() == KeyCode.SPACE){
          gameUtils.createBulletFor(player, false);
          socketClient.sendMessage("shot");
      }
    };



    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        gameUtils = new GameUtils();


        buttonConnect.setOnAction(event -> {
                socketClient = new SocketClient(this, "localhost", 7777);
                new Thread(socketClient).start();
                buttonConnect.setDisable(true);
                buttonGo.setDisable(false);
                textPlayerName.setDisable(false);
                gameUtils.setPane(pane);
                gameUtils.setClient(socketClient);
        });
        buttonGo.setOnAction(event ->{

        socketClient.sendMessage("name: " + textPlayerName.getText());
        buttonGo.setDisable(true);
        textPlayerName.setDisable(true);
        buttonGo.getScene().getRoot().requestFocus();
        });

        gameUtils.setController(this);

    }

    public Circle getEnemy() {
        return enemy;
    }

    public Circle getPlayer() {
        return player;
    }

    public Label getHpPlayer() {
        return hpPlayer;
    }

    public Label getHpEnemy() {
        return hpEnemy;
    }

    public Label getFinishStatistic() {
        return finishStatistic;
    }

    public void setFinishStatistic(Label finishStatistic) {
        this.finishStatistic = finishStatistic;
    }
}
