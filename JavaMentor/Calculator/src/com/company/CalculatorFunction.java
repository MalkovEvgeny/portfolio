package com.company;

public interface CalculatorFunction {

    int addition(int[] numbers);
    int subtraction(int[] numbers);
    int division(int[] numbers);
    int multiplication(int[] numbers);
}
