package com.company;

public class CalculatorFunctionImpl implements CalculatorFunction{

    @Override
    public int addition(int[] numbers) {
        int i = numbers[0] + numbers[1];
        return i;
    }

    @Override
    public int subtraction(int[] numbers) {
        int i = numbers[0] - numbers[1];
        return i;
    }

    @Override
    public int division(int[] numbers) {
        int i = numbers[0] / numbers[1];
        return i;
    }

    @Override
    public int multiplication(int[] numbers) {
        int i = numbers[0] * numbers[1];
        return i;
    }
}
