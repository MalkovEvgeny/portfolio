package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        CalculatorFunction calculatorFunction = new CalculatorFunctionImpl();
        Parser parser = new Parser();
        Scanner scanner = new Scanner(System.in);

        String s = scanner.nextLine();
        int[] numbersArray = parser.numberParser(s);

        String result = parser.operationParser(s);
        int resultInt = 0;

        if(result.equals("additional")){
            resultInt = calculatorFunction.addition(numbersArray);
        }else if (result.equals("subtraction")){
            resultInt = calculatorFunction.subtraction(numbersArray);
        }else if (result.equals("division")){
            resultInt = calculatorFunction.division(numbersArray);
        }else if (result.equals("multiplication")){
            resultInt = calculatorFunction.multiplication(numbersArray);
        }

        System.out.println(parser.resultParser(resultInt));



    }
}
