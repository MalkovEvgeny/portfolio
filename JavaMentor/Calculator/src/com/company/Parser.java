package com.company;


import java.util.HashMap;
import java.util.Map;

public class Parser {
    private int checkRoman = 0;

    String resultParser(int resultInt){
        if(checkRoman != 0){
            if(resultInt <= 0){
                return "Sorry, у Римлян не было цифр ниже единицы";
            }
            Map<Integer, String> map = new HashMap<>();
            map.put(1,"I");
            map.put(2,"II");
            map.put(3,"III");
            map.put(4,"IV");
            map.put(5,"V");
            map.put(6,"VI");
            map.put(7,"VII");
            map.put(8,"VIII");
            map.put(9,"IX");
            map.put(10,"X");
            map.put(11,"XI");
            map.put(12,"XII");
            map.put(13,"XIII");
            map.put(14,"XIV");
            map.put(15,"XV");
            map.put(16,"XVI");
            map.put(17,"XVII");
            map.put(18,"XVIII");
            map.put(19,"XIX");
            map.put(20,"XX");

            return map.get(resultInt);
        }
        return String.valueOf(resultInt);
    }

    String operationParser(String s){
        for (String parsedString : s.split(" ")){
            if (parsedString.equals("+")){
                return "additional";
            }else if (parsedString.equals("-")){
                return "subtraction";
            }else if (parsedString.equals("/")){
                return "division";
            }else if (parsedString.equals("*")){
                return "multiplication";
            }
        }
        return null;
    }


    int[] numberParser(String s){
        int[] numbersArray = new int[2];
        int[] arabNumbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        String[] romanNumbers = {"I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X"};
        int position = 0;

        for (String parsedString : s.split(" ")){
            for (int i = 0; i < romanNumbers.length; i++){
                if (parsedString.equals(romanNumbers[i])) {
                    numbersArray[position] = i + 1;
                    position++;
                    checkRoman++;
                    break;
                }
            }
        }
        if (checkRoman == 0){
            int j = 0;
            for (String parsedString : s.split(" ")) {
                if (j != 1) {
                    for (int i = 0; i < arabNumbers.length; i++) {
                        int number = Integer.parseInt(parsedString);
                        if (number == arabNumbers[i]) {
                            numbersArray[position] = number;
                            position++;
                            break;
                        }
                    }
                }
                j++;
            }
        }
        for (int i = 0; i < numbersArray.length; i++){
            if(numbersArray[i] > 10 || numbersArray[i] < 1){
                System.err.println("Введен неверный формат числа");
                System.exit(0);
            }
        }
        return numbersArray;
    }

}
