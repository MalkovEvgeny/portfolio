GameIntroMaven and JavaFXGameClient
This is game in which I used next Java Technologies:
1. Java SE
2. Socket IO
3. Streams API
4. Java FX
5. JDBC - PostgresQL
6. Threads
7. Apache Maven

Manual:
1. Run project GameIntroMaven (this is gameServer)
2. Run project JavaFXGameClient - first player
2.1 Run project JavaFXGameClient - second player
3. Push the button "Подключиться" - first/second player
4. Game is started 
