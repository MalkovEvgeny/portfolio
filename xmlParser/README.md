This task I got from potential employer.
Project must realize next functional:
1. After Starting the program you should enter number N.
2. Generates a valid XML document of the form 1...N :
<entries>
   <entry>
        <field> 1 </field>
   </entry>
   ....
   <entry>
        <field> N </field>
   </entry>
</entries>
   
Document save in system how 1.xml

3. With XSLT program refactor 1.xml to next form:
<entries>
   <entry field="1">
   ...
   <entry field="N">
</entries
   
New document save on system how 2.xml

4. Program parsing 2.xml and show average value to display
