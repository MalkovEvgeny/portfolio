package app;

import java.util.Objects;

public class DBConnection {
    private String jdbc_url = "jdbc:postgresql://localhost:5432/testDB";
    private String jdbc_user = "postgres";
    private String jdbc_password = "nt7kaspy";
    private int n;

    public DBConnection() {
    }

    public String getJdbc_url() {
        return jdbc_url;
    }

    public String setJdbc_url(String jdbc_url) {
        this.jdbc_url = jdbc_url;
        return jdbc_url;
    }

    public String getJdbc_user() {
        return jdbc_user;
    }

    public String setJdbc_user(String jdbc_user) {
        this.jdbc_user = jdbc_user;
        return jdbc_user;
    }

    public String getJdbc_password() {
        return jdbc_password;
    }

    public String setJdbc_password(String jdbc_password) {
        this.jdbc_password = jdbc_password;
        return jdbc_password;
    }

    public int getN() {
        return n;
    }

    public int setN(int n) {
        this.n = n;
        return n;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DBConnection DBConnection = (DBConnection) o;
        return n == DBConnection.n && Objects.equals(jdbc_url, DBConnection.jdbc_url) && Objects.equals(jdbc_user, DBConnection.jdbc_user) && Objects.equals(jdbc_password, DBConnection.jdbc_password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jdbc_url, jdbc_user, jdbc_password, n);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DataBean{");
        sb.append("jdbc_url='").append(jdbc_url).append('\'');
        sb.append(", jdbc_user='").append(jdbc_user).append('\'');
        sb.append(", jdbc_password='").append(jdbc_password).append('\'');
        sb.append(", n=").append(n);
        sb.append('}');
        return sb.toString();
    }
}