package app;

import com.sun.xml.internal.txw2.output.IndentingXMLStreamWriter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.*;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    private static final ArrayList<Integer> N_COUNT = new ArrayList<>();
    private static final String SQL_INSERT = "insert into test(field) values (?)";
    private static final String SQL_DELETE = "delete from test where field >= 0";
    private static final String SQL_SELECT = "select field from test";
    private static final String FILE_NAME_FIRST = "1.xml";
    private static final String FILE_NAME_SECOND = "2.xml";

    public static void main(String[] args) {
        DBConnection DBConnection = new DBConnection();
        Scanner scanner = new Scanner(System.in);

        try {
            Connection connection = connectSQL(DBConnection);
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT);
            PreparedStatement deletePreparedStatement = connection.prepareStatement(SQL_DELETE);
            PreparedStatement selectPreparedStatement = connection.prepareStatement(SQL_SELECT);
            deletePreparedStatement.executeUpdate();
            System.out.println("Введите значение N: ");

            int n = DBConnection.setN(scanner.nextInt());

            for (int i = 1; i <= n; i++){
                preparedStatement.setInt(1, i);
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();


            List<Integer> arrayN = new ArrayList<>();

            try (ResultSet resultSet = selectPreparedStatement.executeQuery()) {
                while (resultSet.next()){
                    arrayN.add(resultSet.getInt(1));
                }
            }

            writeToXML(arrayN);

            transformationToXsl();

            parseToInteger();

            getAverage();


        } catch (SQLException | IOException | XMLStreamException | TransformerException | ParserConfigurationException | SAXException e) {
            throw new IllegalStateException(e);
        }

    }

    private static void parseToInteger() throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        SAXParser parser = parserFactory.newSAXParser();
        XMLHandler handler = new XMLHandler();
        parser.parse(new File(FILE_NAME_SECOND), handler);
    }

    private static void getAverage() {
        double sumN = 0;
        double average = 0;
        for(int i = 0; i < N_COUNT.size(); i++){
            sumN = N_COUNT.get(i) + sumN;
        }
        average = sumN / (N_COUNT.size());
        System.out.println(average);
    }

    private static void transformationToXsl() throws TransformerException {
        TransformerFactory factory = TransformerFactory.newInstance();
        Source xslt = new StreamSource(new File("document.xsl"));
        Transformer transformer = factory.newTransformer(xslt);
        Source xml = new StreamSource(new File(FILE_NAME_FIRST));
        transformer.transform(xml, new StreamResult(new File (FILE_NAME_SECOND)));
    }

    private static void writeToXML(List<Integer> arrayN) throws XMLStreamException, IOException {
        XMLOutputFactory output = XMLOutputFactory.newInstance();
        XMLStreamWriter writer = new IndentingXMLStreamWriter(output.createXMLStreamWriter(new FileWriter(FILE_NAME_FIRST)));
        writer.writeStartElement("entries");
        for (int i = 0; i < arrayN.size(); i++){
            writer.writeStartElement("entry");
            writer.writeStartElement("field");
            writer.writeCharacters(arrayN.get(i).toString());
            writer.writeEndElement();
            writer.writeEndElement();
        }
        writer.writeEndElement();
        writer.flush();
        writer.close();
    }


    private static Connection connectSQL(DBConnection DBConnection) throws SQLException {
        return DriverManager.getConnection(DBConnection.getJdbc_url(),
                DBConnection.getJdbc_user(),
                DBConnection.getJdbc_password());
    }

    private static class XMLHandler extends DefaultHandler{
        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if(qName.equals("entry")){
                Integer n = Integer.valueOf(attributes.getValue("field"));
                N_COUNT.add(n);
            }
        }
    }
}
